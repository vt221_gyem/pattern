class Animal:
    def __init__(self, species, name, age):
        self.species = species
        self.name = name
        self.age = age

    def __str__(self):
        return f"{self.name} the {self.species}, {self.age} years old"


class Mammal(Animal):
    def __init__(self, species, name, age, is_marine):
        super().__init__(species, name, age)
        self.is_marine = is_marine


class Bird(Animal):
    def __init__(self, species, name, age, can_fly):
        super().__init__(species, name, age)
        self.can_fly = can_fly


class Reptile(Animal):
    def __init__(self, species, name, age, is_venomous):
        super().__init__(species, name, age)
        self.is_venomous = is_venomous


class Enclosure:
    def __init__(self, enclosure_id, size, enclosure_type):
        self.enclosure_id = enclosure_id
        self.size = size
        self.enclosure_type = enclosure_type
        self.animals = []

    def add_animal(self, animal):
        self.animals.append(animal)

    def __str__(self):
        return f"Enclosure {self.enclosure_id} ({self.enclosure_type}, {self.size} m²) with {len(self.animals)} animals"


class Food:
    def __init__(self, food_type, quantity):
        self.food_type = food_type
        self.quantity = quantity

    def __str__(self):
        return f"{self.food_type}: {self.quantity} kg"


class ZooEmployee:
    def __init__(self, employee_id, name, position):
        self.employee_id = employee_id
        self.name = name
        self.position = position

    def __str__(self):
        return f"{self.name}, {self.position}"


class Zoo:
    def __init__(self):
        self.enclosures = []
        self.food_stock = []
        self.employees = []

    def add_enclosure(self, enclosure):
        self.enclosures.append(enclosure)

    def add_food(self, food):
        self.food_stock.append(food)

    def add_employee(self, employee):
        self.employees.append(employee)

    def inventory(self):
        print("Zoo Inventory:")
        print("\nEnclosures:")
        for enclosure in self.enclosures:
            print(enclosure)
            for animal in enclosure.animals:
                print(f"  - {animal}")

        print("\nFood Stock:")
        for food in self.food_stock:
            print(food)

        print("\nEmployees:")
        for employee in self.employees:
            print(employee)


# Приклад використання

# Створення тварин
lion = Mammal("Lion", "Simba", 5, False)
eagle = Bird("Eagle", "Eddie", 3, True)
snake = Reptile("Python", "Slytherin", 2, True)

# Створення вольєрів
enclosure1 = Enclosure(1, 500, "Savannah")
enclosure2 = Enclosure(2, 300, "Aviary")
enclosure3 = Enclosure(3, 200, "Reptile House")

# Додавання тварин до вольєрів
enclosure1.add_animal(lion)
enclosure2.add_animal(eagle)
enclosure3.add_animal(snake)

# Створення корму
meat = Food("Meat", 50)
fish = Food("Fish", 30)

# Створення працівників зоопарку
keeper = ZooEmployee(1, "John Doe", "Animal Keeper")
vet = ZooEmployee(2, "Jane Smith", "Veterinarian")

# Створення зоопарку та додавання елементів
zoo = Zoo()
zoo.add_enclosure(enclosure1)
zoo.add_enclosure(enclosure2)
zoo.add_enclosure(enclosure3)
zoo.add_food(meat)
zoo.add_food(fish)
zoo.add_employee(keeper)
zoo.add_employee(vet)

# Інвентаризація
zoo.inventory()

def main():
    # Створення тварин
    lion = Mammal("Lion", "Simba", 5, False)
    eagle = Bird("Eagle", "Eddie", 3, True)
    snake = Reptile("Python", "Slytherin", 2, True)
    elephant = Mammal("Elephant", "Dumbo", 10, False)

    # Створення вольєрів
    enclosure1 = Enclosure(1, 500, "Savannah")
    enclosure2 = Enclosure(2, 300, "Aviary")
    enclosure3 = Enclosure(3, 200, "Reptile House")
    enclosure4 = Enclosure(4, 800, "Grassland")

    # Додавання тварин до вольєрів
    enclosure1.add_animal(lion)
    enclosure2.add_animal(eagle)
    enclosure3.add_animal(snake)
    enclosure4.add_animal(elephant)

    # Створення корму
    meat = Food("Meat", 50)
    fish = Food("Fish", 30)
    grass = Food("Grass", 100)

    # Створення працівників зоопарку
    keeper = ZooEmployee(1, "John Doe", "Animal Keeper")
    vet = ZooEmployee(2, "Jane Smith", "Veterinarian")
    guide = ZooEmployee(3, "Alice Johnson", "Tour Guide")

    # Створення зоопарку та додавання елементів
    zoo = Zoo()
    zoo.add_enclosure(enclosure1)
    zoo.add_enclosure(enclosure2)
    zoo.add_enclosure(enclosure3)
    zoo.add_enclosure(enclosure4)
    zoo.add_food(meat)
    zoo.add_food(fish)
    zoo.add_food(grass)
    zoo.add_employee(keeper)
    zoo.add_employee(vet)
    zoo.add_employee(guide)

    # Інвентаризація
    zoo.inventory()

if __name__ == "__main__":
    main()
