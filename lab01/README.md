# Zoo Management System

This project demonstrates a zoo management system in Python that adheres to various programming principles.

## Demonstrated Principles

### 1. DRY (Don't Repeat Yourself)
The code avoids repetition by using inheritance for different types of animals (Mammals, Birds, Reptiles), which share common properties and behaviors.

**Example:**
- Common attributes and methods are defined in the base `Animal` class and inherited by subclasses `Mammal`, `Bird`, and `Reptile`.
- [Definition of `Animal` class](main.py#L1)
- [Definition of `Mammal` class](main.py#L10)
- [Definition of `Bird` class](main.py#L16)
- [Definition of `Reptile` class](main.py#L22)

### 2. KISS (Keep It Simple, Stupid)
The design of classes and methods is simple and straightforward. Each class has a single responsibility, and methods are short and focused.

**Example:**
- The `Enclosure` class is responsible for adding animals and storing them in a list.
- [Definition of `Enclosure` class](main.py#L28)

### 3. SOLID Principles

#### Single Responsibility Principle (SRP)
Each class in the system has a single responsibility.

**Example:**
- The `Animal` class is responsible for animal attributes.
- The `Enclosure` class is responsible for managing enclosures and the animals within them.
- [Definition of `Animal` class](main.py#L1)
- [Definition of `Enclosure` class](main.py#L28)

#### Open/Closed Principle (OCP)
Classes are open for extension but closed for modification. New types of animals can be added by creating new subclasses of `Animal`.

**Example:**
- The system can be extended with more specific animal types without modifying existing code.
- [Definition of `Animal` class](main.py#L1)
- [Definition of subclasses](main.py#L10)

#### Liskov Substitution Principle (LSP)
Subtypes must be substitutable for their base types. `Mammal`, `Bird`, and `Reptile` can be used where `Animal` is expected.

**Example:**
- The `Enclosure` class can handle any `Animal` object, including `Mammal`, `Bird`, or `Reptile`.
- [Definition of `Enclosure` class](main.py#L28)
- [Adding animals to enclosures](main.py#L62)

#### Interface Segregation Principle (ISP)
Directly not applicable here, as Python does not have interfaces, but each class has only the necessary methods and attributes for its functionality.

**Example:**
- The `ZooEmployee` class has only those attributes and methods that pertain to an employee.
- [Definition of `ZooEmployee` class](main.py#L44)

#### Dependency Inversion Principle (DIP)
High-level modules should not depend on low-level modules; both should depend on abstractions. This principle is less applicable in this context but can be seen in how the `Zoo` class manages its components (enclosures, food, employees) through abstraction.

**Example:**
- The `Zoo` class depends on the abstract behavior of `Enclosure`, `Food`, and `ZooEmployee`.
- [Definition of `Zoo` class](main.py#L50)

### 4. YAGNI (You Aren't Gonna Need It)
The system includes only the functionality that is currently necessary, avoiding over-engineering.

**Example:**
- Classes have only the attributes and methods needed for the current requirements of the zoo management system.
- [Definition of `Zoo` class](main.py#L50)

### 5. Composition over Inheritance
The system uses composition to manage relationships between classes. For example, `Zoo` contains a list of `Enclosure`, `Food`, and `ZooEmployee` objects.

**Example:**
- The `Zoo` class is composed of various other objects (enclosures, food, employees).
- [Definition of `Zoo` class](main.py#L50)

### 6. Program to Interfaces, not Implementations
Classes interact with each other through their interfaces (public methods and attributes) rather than through specific implementations.

**Example:**
- The `Zoo` class interacts with `Enclosure` objects through their public methods.
- [Definition of `Zoo` class](main.py#L50)
- [Definition of `Enclosure` class](main.py#L28)

### 7. Fail Fast
The system is designed to quickly detect errors, although explicit error handling is minimal in this example.

**Example:**
- Ensuring only valid objects are added to lists in `Zoo`.
- [Adding animals to enclosures](main.py#L62)

## Running the Code
To run the code and see the zoo inventory, execute the following command:

```sh
python main.py
