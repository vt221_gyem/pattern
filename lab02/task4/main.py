import copy


class Virus:
    def __init__(self, weight, age, name, species, children=[]):
        self.weight = weight
        self.age = age
        self.name = name
        self.species = species
        self.children = children

    def clone(self):
        # Deep copy the virus object
        cloned_virus = copy.deepcopy(self)
        # Deep copy the children viruses
        cloned_virus.children = [child.clone() for child in self.children]
        return cloned_virus


def main():
    # Creating a family of viruses
    child1 = Virus(0.1, 1, "Child1", "Coronavirus")
    child2 = Virus(0.2, 2, "Child2", "Influenza")
    child3 = Virus(0.3, 3, "Child3", "Ebola")

    parent = Virus(1, 5, "Parent", "COVID-19", [child1, child2, child3])

    # Cloning the parent virus
    cloned_parent = parent.clone()

    # Checking if cloning is successful
    print("Cloned parent virus:")
    print(cloned_parent.weight, cloned_parent.age, cloned_parent.name, cloned_parent.species)
    for child in cloned_parent.children:
        print(child.weight, child.age, child.name, child.species)


if __name__ == "__main__":
    main()
