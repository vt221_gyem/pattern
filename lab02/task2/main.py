from abc import ABC, abstractmethod

# Abstract Product A: Device
class Device(ABC):
    @abstractmethod
    def get_specs(self):
        pass

# Concrete Product A1: Laptop
class Laptop(Device):
    def get_specs(self):
        return "Laptop"

# Concrete Product A2: Netbook
class Netbook(Device):
    def get_specs(self):
        return "Netbook"

# Concrete Product A3: EBook
class EBook(Device):
    def get_specs(self):
        return "EBook"

# Concrete Product A4: Smartphone
class Smartphone(Device):
    def get_specs(self):
        return "Smartphone"

# Abstract Product B: Brand
class Brand(ABC):
    @abstractmethod
    def get_name(self):
        pass

# Concrete Product B1: IProne
class IProne(Brand):
    def get_name(self):
        return "IProne"

# Concrete Product B2: Kiaomi
class Kiaomi(Brand):
    def get_name(self):
        return "Kiaomi"

# Concrete Product B3: Balaxy
class Balaxy(Brand):
    def get_name(self):
        return "Balaxy"

# Abstract Factory: Tech Factory
class TechFactory(ABC):
    @abstractmethod
    def create_device(self):
        pass

    @abstractmethod
    def create_brand(self):
        pass

# Concrete Factory 1: IProne Factory
class IProneFactory(TechFactory):
    def create_device(self):
        return Smartphone()

    def create_brand(self):
        return IProne()

# Concrete Factory 2: Kiaomi Factory
class KiaomiFactory(TechFactory):
    def create_device(self):
        return Laptop()

    def create_brand(self):
        return Kiaomi()

# Concrete Factory 3: Balaxy Factory
class BalaxyFactory(TechFactory):
    def create_device(self):
        return EBook()

    def create_brand(self):
        return Balaxy()

def main():
    # Create IProne devices
    iprone_factory = IProneFactory()
    iprone_device = iprone_factory.create_device()
    iprone_brand = iprone_factory.create_brand()
    print(f"{iprone_brand.get_name()} {iprone_device.get_specs()}")

    # Create Kiaomi devices
    kiaomi_factory = KiaomiFactory()
    kiaomi_device = kiaomi_factory.create_device()
    kiaomi_brand = kiaomi_factory.create_brand()
    print(f"{kiaomi_brand.get_name()} {kiaomi_device.get_specs()}")

    # Create Balaxy devices
    balaxy_factory = BalaxyFactory()
    balaxy_device = balaxy_factory.create_device()
    balaxy_brand = balaxy_factory.create_brand()
    print(f"{balaxy_brand.get_name()} {balaxy_device.get_specs()}")

if __name__ == "__main__":
    main()
