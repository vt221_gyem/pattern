from abc import ABC, abstractmethod

class Subscription:
    def __init__(self, monthly_fee, minimum_period, channels, features):
        self.monthly_fee = monthly_fee
        self.minimum_period = minimum_period
        self.channels = channels
        self.features = features

    def __str__(self):
        return (f"Subscription: {self.__class__.__name__}\n"
                f"Monthly Fee: ${self.monthly_fee}\n"
                f"Minimum Period: {self.minimum_period} months\n"
                f"Channels: {', '.join(self.channels)}\n"
                f"Features: {', '.join(self.features)}")

class DomesticSubscription(Subscription):
    def __init__(self):
        super().__init__(monthly_fee=10, minimum_period=6,
                         channels=["News", "Entertainment", "Sports"],
                         features=["Basic Support"])

class EducationalSubscription(Subscription):
    def __init__(self):
        super().__init__(monthly_fee=5, minimum_period=3,
                         channels=["Documentaries", "Science", "Education"],
                         features=["Educational Resources", "Basic Support"])

class PremiumSubscription(Subscription):
    def __init__(self):
        super().__init__(monthly_fee=20, minimum_period=12,
                         channels=["All Channels"],
                         features=["Premium Support", "No Ads", "4K Streaming"])

class SubscriptionFactory(ABC):
    @abstractmethod
    def create_subscription(self):
        pass

class WebSiteFactory(SubscriptionFactory):
    def create_subscription(self, subscription_type):
        if subscription_type == 'Domestic':
            return DomesticSubscription()
        elif subscription_type == 'Educational':
            return EducationalSubscription()
        elif subscription_type == 'Premium':
            return PremiumSubscription()
        else:
            raise ValueError("Unknown subscription type")

class MobileAppFactory(SubscriptionFactory):
    def create_subscription(self, subscription_type):
        if subscription_type == 'Domestic':
            return DomesticSubscription()
        elif subscription_type == 'Educational':
            return EducationalSubscription()
        elif subscription_type == 'Premium':
            return PremiumSubscription()
        else:
            raise ValueError("Unknown subscription type")

class ManagerCallFactory(SubscriptionFactory):
    def create_subscription(self, subscription_type):
        if subscription_type == 'Domestic':
            return DomesticSubscription()
        elif subscription_type == 'Educational':
            return EducationalSubscription()
        elif subscription_type == 'Premium':
            return PremiumSubscription()
        else:
            raise ValueError("Unknown subscription type")

def main():
    # Create factories
    website_factory = WebSiteFactory()
    mobile_app_factory = MobileAppFactory()
    manager_call_factory = ManagerCallFactory()

    # Create subscriptions using different factories
    domestic_subscription_website = website_factory.create_subscription('Domestic')
    educational_subscription_mobile = mobile_app_factory.create_subscription('Educational')
    premium_subscription_manager = manager_call_factory.create_subscription('Premium')

    # Print the details of the subscriptions
    print(domestic_subscription_website)
    print()
    print(educational_subscription_mobile)
    print()
    print(premium_subscription_manager)

if __name__ == "__main__":
    main()
