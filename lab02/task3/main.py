import threading

class Authenticator:
    _instance = None
    _lock = threading.Lock()

    def __new__(cls, *args, **kwargs):
        with cls._lock:
            if not cls._instance:
                cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def authenticate(self, entered_username, entered_password):
        return self.username == entered_username and self.password == entered_password

def main():
    # Creating instances of Authenticator
    auth1 = Authenticator("admin", "password123")
    auth2 = Authenticator("root", "admin@123")

    # Both auth1 and auth2 point to the same object
    print(auth1 is auth2)  # Output: True

    # Testing authentication
    print(auth1.authenticate("admin", "password123"))  # Output: True
    print(auth2.authenticate("root", "admin@123"))     # Output: True

if __name__ == "__main__":
    main()
