class Hero:
    def __init__(self):
        self.height = None
        self.build = None
        self.hair_color = None
        self.eye_color = None
        self.outfit = None
        self.inventory = []

    def __str__(self):
        return f"Hero: \nHeight: {self.height}\nBuild: {self.build}\nHair Color: {self.hair_color}\nEye Color: {self.eye_color}\nOutfit: {self.outfit}\nInventory: {', '.join(self.inventory)}"

class HeroBuilder:
    def __init__(self):
        self.hero = Hero()

    def set_height(self, height):
        self.hero.height = height
        return self

    def set_build(self, build):
        self.hero.build = build
        return self

    def set_hair_color(self, hair_color):
        self.hero.hair_color = hair_color
        return self

    def set_eye_color(self, eye_color):
        self.hero.eye_color = eye_color
        return self

    def set_outfit(self, outfit):
        self.hero.outfit = outfit
        return self

    def add_to_inventory(self, item):
        self.hero.inventory.append(item)
        return self

    def get_hero(self):
        return self.hero

class EnemyBuilder(HeroBuilder):
    def __init__(self):
        super().__init__()

    def create_evil(self):
        self.hero.hair_color = "Black"
        self.hero.eye_color = "Red"
        self.hero.outfit = "Dark Armor"
        self.hero.inventory.append("Evil Sword")
        return self

def main():
    # Creating a hero using HeroBuilder
    hero_builder = HeroBuilder()
    hero = hero_builder.set_height("Tall") \
                        .set_build("Athletic") \
                        .set_hair_color("Blonde") \
                        .set_eye_color("Blue") \
                        .set_outfit("Knight Armor") \
                        .add_to_inventory("Sword") \
                        .add_to_inventory("Shield") \
                        .get_hero()
    print("Created Hero:")
    print(hero)

    # Creating an enemy using EnemyBuilder
    enemy_builder = EnemyBuilder()
    enemy = enemy_builder.set_height("Average") \
                          .set_build("Muscular") \
                          .create_evil() \
                          .add_to_inventory("Poison") \
                          .get_hero()
    print("\nCreated Enemy:")
    print(enemy)

if __name__ == "__main__":
    main()
