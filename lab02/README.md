## Overview
This project showcases the implementation of several design patterns in Python. The project is organized into different sections, each demonstrating a specific design pattern through concrete examples.

## Implemented Design Patterns

### 1. Factory Method Pattern
**File:** `main.py`

**Description:**
The Factory Method pattern is used to create objects without specifying the exact class of object that will be created. This pattern is demonstrated through the creation of different types of subscriptions.

**Classes:**
- `Subscription`: Base class for different types of subscriptions.
- `DomesticSubscription`, `EducationalSubscription`, `PremiumSubscription`: Concrete subscription classes.
- `SubscriptionFactory`: Abstract factory class for creating subscriptions.
- `WebSiteFactory`, `MobileAppFactory`, `ManagerCallFactory`: Concrete factory classes for creating subscriptions via different mediums.

---

### 2. Abstract Factory Pattern
**File:** `main.py`

**Description:**
The Abstract Factory pattern provides an interface for creating families of related or dependent objects without specifying their concrete classes. This pattern is illustrated by creating various tech products and their respective brands.

**Classes:**
- `Device`: Abstract product for devices.
- `Laptop`, `Netbook`, `EBook`, `Smartphone`: Concrete device classes.
- `Brand`: Abstract product for brands.
- `IProne`, `Kiaomi`, `Balaxy`: Concrete brand classes.
- `TechFactory`: Abstract factory class for creating tech products.
- `IProneFactory`, `KiaomiFactory`, `BalaxyFactory`: Concrete factory classes for creating specific tech products.

---

### 3. Singleton Pattern
**File:** `main.py`

**Description:**
The Singleton pattern ensures that a class has only one instance and provides a global point of access to that instance. This pattern is used in the authenticator class to ensure only one instance of the authenticator is created.

**Classes:**
- `Authenticator`: Singleton class that ensures only one instance is created.

---

### 4. Prototype Pattern
**File:** `main.py`

**Description:**
The Prototype pattern is used to create new objects by copying an existing object, known as the prototype. This pattern is implemented in the virus class to support cloning (deep copy).

**Classes:**
- `Virus`: Class that supports cloning (deep copy).

---

### 5. Builder Pattern
**File:** `main.py`

**Description:**
The Builder pattern separates the construction of a complex object from its representation, allowing the same construction process to create different representations. This pattern is demonstrated by creating hero and enemy characters using builders.

**Classes:**
- `Hero`: Class representing a hero.
- `HeroBuilder`: Builder class for creating Hero objects.
- `EnemyBuilder`: Builder class for creating enemy heroes.

---

## How to Run
To run the examples, execute the `main.py` file. It contains a main function that demonstrates the usage of each design pattern implemented in this project.