import re

class SmartTextReader:
    def read_text(self, filename):
        with open(filename, 'r') as file:
            lines = file.readlines()
            return [list(line.strip()) for line in lines]

class SmartTextChecker:
    def __init__(self, reader):
        self.reader = reader

    def read_text(self, filename):
        print(f"Opening file: {filename}")
        result = self.reader.read_text(filename)
        print(f"Successfully read {len(result)} lines and {sum(len(line) for line in result)} characters")
        print(f"Closing file: {filename}")
        return result

class SmartTextReaderLocker:
    def __init__(self, reader, regex):
        self.reader = reader
        self.regex = regex

    def read_text(self, filename):
        if re.match(self.regex, filename):
            print("Access denied!")
            return None
        else:
            return self.reader.read_text(filename)

def main():
    reader = SmartTextReader()
    checker_proxy = SmartTextChecker(reader)
    locker_proxy = SmartTextReaderLocker(checker_proxy, r'.*restricted.*')

    # Reading text files
    print("Reading allowed files:")
    print(locker_proxy.read_text("text1.txt"))
    print(locker_proxy.read_text("text2.txt"))

    # Reading restricted file
    print("\nTrying to read restricted file:")
    print(locker_proxy.read_text("restricted_file.txt"))

if __name__ == "__main__":
    main()
