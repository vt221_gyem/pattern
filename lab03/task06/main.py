import sys
class LightHTML:
    def __init__(self):
        self.output = ""

    def process_line(self, line):
        if len(line.strip()) == 0:
            return
        elif len(line) < 20:
            self.output += f"<h2>{line}</h2>"
        elif line[0].isspace():
            self.output += f"<blockquote>{line.strip()}</blockquote>"
        else:
            self.output += f"<p>{line}</p>"

    def generate_html(self, text):
        lines = text.split('\n')
        self.output = ""
        for line in lines:
            self.process_line(line)
        return f"<h1>{lines[0]}</h1>{self.output}"

# Головна функція програми
def main():
    # Вхідний текст книги
    text = """
    Текст книги тут...
    """

    # Створення екземпляру класу
    light_html = LightHTML()

    # Генерування HTML верстки
    html_output = light_html.generate_html(text)

    # Виведення HTML верстки
    print(html_output)

    # Розмір у байтах, який займає наша HTML верстка в пам'яті
    html_size = sys.getsizeof(html_output)
    print("Розмір HTML верстки в пам'яті:", html_size, "байт")

# Виклик головної функції
if __name__ == "__main__":
    main()
